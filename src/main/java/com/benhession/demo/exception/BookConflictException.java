package com.benhession.demo.exception;

public class BookConflictException extends Exception {
    public BookConflictException(String message) {
        super(message);
    }
}
