package com.benhession.demo.repository;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;

import org.springframework.stereotype.Repository;

import com.benhession.demo.exception.BookConflictException;
import com.benhession.demo.model.Book;

@Repository
public class BookRepository {
    private final Set<Book> books = new HashSet<>();
    private final AtomicInteger index = new AtomicInteger(1);

    public Book save(Book book) throws BookConflictException {
        if (books.add(book)) {
            book.setId(index.getAndIncrement());
            return book;
        }

        throw new BookConflictException("Book already present");
    }

    public Set<Book> findAll() {
        return new HashSet<>(this.books);
    }
}
