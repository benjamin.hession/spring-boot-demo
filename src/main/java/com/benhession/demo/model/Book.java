package com.benhession.demo.model;

import com.benhession.demo.dto.AddBookDTO;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
public class Book {
    @EqualsAndHashCode.Exclude
    private int id;
    private String title;
    private String author;

    public Book(AddBookDTO addBookDTO) {
        this.title = addBookDTO.getTitle();
        this.author = addBookDTO.getAuthor();
    }
}
