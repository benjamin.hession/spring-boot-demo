package com.benhession.demo.controller;

import static org.springframework.http.HttpStatus.CONFLICT;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.benhession.demo.dto.AddBookDTO;
import com.benhession.demo.model.Book;
import com.benhession.demo.service.BookService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(path = "/book")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BookController {

    private final BookService bookService;

    @PostMapping
    public ResponseEntity<Book> addBook(@RequestBody AddBookDTO addBookDTO) {
        Optional<Book> savedBook = bookService.addBook(addBookDTO);

        return savedBook
            .map(ResponseEntity::ok)
            .orElseGet(() -> ResponseEntity.status(CONFLICT).build());
    }

    @GetMapping
    public ResponseEntity<List<Book>> getAllBooks() {
        List<Book> booksByTitle = bookService.allBooksSortedByTitle();

        if (booksByTitle.isEmpty()) {
            return ResponseEntity.noContent().build();
        }

        return ResponseEntity.ok(booksByTitle);
    }
}
