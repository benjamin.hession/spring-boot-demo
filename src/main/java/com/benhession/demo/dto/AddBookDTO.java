package com.benhession.demo.dto;

import lombok.Data;

@Data
public class AddBookDTO {
    private String title;
    private String author;
}
