package com.benhession.demo.service;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.benhession.demo.dto.AddBookDTO;
import com.benhession.demo.exception.BookConflictException;
import com.benhession.demo.model.Book;
import com.benhession.demo.repository.BookRepository;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class BookService {

    private final BookRepository bookRepository;

    public Optional<Book> addBook(AddBookDTO addBookDTO) {
        Book book = new Book(addBookDTO);
        try {
            return Optional.of(bookRepository.save(book));
        } catch (BookConflictException e) {
            return Optional.empty();
        }
    }

    public List<Book> allBooksSortedByTitle() {
        return bookRepository.findAll()
            .stream()
            .sorted(Comparator.comparing(Book::getTitle))
            .toList();
    }
}
